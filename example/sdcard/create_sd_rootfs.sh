#!/bin/bash

#删除制作根文件系统的旧文件夹
sudo rm -rf rootfs

#删除临时挂载点目录
sudo rm -rf tmpfs

#删除以前制作的根文件系统镜像。
sudo rm -f a9rootfs.ext3

#创建制作根文件系统目录
sudo mkdir rootfs

#将busybox生成的文件系统下的内容，全部复制到rootfs目录中
sudo cp busybox1.33.2_install/*  rootfs/ -raf

#补全busybox生成的文件系统中，缺失的其他目录。
sudo mkdir -p rootfs/proc/
sudo mkdir -p rootfs/sys/
sudo mkdir -p rootfs/tmp/
sudo mkdir -p rootfs/root/
sudo mkdir -p rootfs/var/
sudo mkdir -p rootfs/mnt/
sudo mkdir -p rootfs/dev/
sudo mkdir -p rootfs/home/
sudo mkdir -p rootfs/home/root/

#将提供了各种linux启动配置文件的目录etc，拷贝到rootfs目录中
sudo cp etc rootfs/ -arf

#为了支持动态编译的应用程序的执行，根文件系统添加arm64相关的动态库文件到rootfs/lib下。
sudo cp -arf /usr/local/arm-gcc/sysroot-glibc-linaro-2.25-2019.12-aarch64-linux-gnu/lib rootfs/

#删除静态库文件，只保留动态库文件。
sudo rm rootfs/lib/*.a

#对库文件进行瘦身，去除符号表和调试信息，使得库文件变小。
sudo aarch64-linux-gnu-strip rootfs/lib/*

#添加常用设备节点，一些设备的节点号是固定的。
sudo mknod rootfs/dev/tty1 c 4 1
sudo mknod rootfs/dev/tty2 c 4 2
sudo mknod rootfs/dev/tty3 c 4 3
sudo mknod rootfs/dev/tty4 c 4 4
sudo mknod rootfs/dev/console c 5 1
sudo mknod rootfs/dev/null c 1 3

#创建一个32M大小，内容全部填充为0的镜像文件。
sudo dd if=/dev/zero of=a9rootfs.ext3 bs=1M count=32

#将空镜像文件格式化为ext3文件系统。
sudo mkfs.ext3 a9rootfs.ext3

#创建镜像文件临时挂载点目录。
sudo mkdir -p tmpfs

#挂载ext3文件系统到tmpfs目录下。
sudo mount -t ext3 a9rootfs.ext3 tmpfs/ -o loop

#使用busybox编译生成的文件，填充文件系统内容。
sudo cp -r rootfs/*  tmpfs/

#取消镜像文件的挂载。
sudo umount tmpfs
#!/bin/bash

#删除制作根文件系统的旧文件夹
rm -rf rootfs

#删除临时挂载点目录
rm -rf tmpfs

#删除旧的基于ram磁盘模拟的根文件系统镜像。
rm -rf ramdisk*

#创建制作根文件系统目录
mkdir rootfs

#将busybox生成的文件系统下的内容，全部复制到rootfs目录中
cp busybox1.33.2_install/*  rootfs/ -raf

#补全busybox生成的文件系统中，缺失的其他目录。
mkdir -p rootfs/proc/
mkdir -p rootfs/sys/
mkdir -p rootfs/tmp/
mkdir -p rootfs/root/
mkdir -p rootfs/var/
mkdir -p rootfs/mnt/
mkdir -p rootfs/dev/
mkdir -p rootfs/home/
mkdir -p rootfs/home/root/

#将提供了各种linux启动配置文件的目录etc，拷贝到rootfs目录中
cp etc rootfs/ -arf

#创建系统动态库保存目录。
mkdir -p rootfs/lib

#加载器ld-2.25.so和ld-linux-aarch64.so.1在动态程序启动前，被用来加载动态库。
cp -arf /usr/local/arm-gcc/sysroot-glibc-linaro-2.25-2019.12-aarch64-linux-gnu/lib/ld-2.25.so rootfs/lib/

#移植arm64版本的libc库到开发板中。
cp -arf /usr/local/arm-gcc/sysroot-glibc-linaro-2.25-2019.12-aarch64-linux-gnu/lib/libc.so.6 rootfs/lib/

#对库文件进行瘦身，去除符号表和调试信息，使得库文件变小。
aarch64-linux-gnu-strip rootfs/lib/*

#创建动态库加载器软衔接。
cd rootfs/lib && ln -s ld-2.25.so ld-linux-aarch64.so.1 && cd -

#添加常用设备节点，一些设备的节点号是固定的。
mknod rootfs/dev/tty1 c 4 1
mknod rootfs/dev/tty2 c 4 2
mknod rootfs/dev/tty3 c 4 3
mknod rootfs/dev/tty4 c 4 4
mknod rootfs/dev/console c 5 1
mknod rootfs/dev/null c 1 3

#创建一个32M大小，内容全部填充为0的镜像文件。
dd if=/dev/zero of=ramdisk.ext4 bs=1M count=32

#将空镜像文件格式化为ext4文件系统。
mkfs.ext4 -F ramdisk.ext4

#创建镜像文件临时挂载点目录。
sudo mkdir -p tmpfs

#挂载ext4镜像文件到tmpfs目录下。
mount -t ext4 ramdisk.ext4 ./tmpfs/  -o loop

#将busybox编译生成的文件全部拷贝到挂载的镜像文件中。
cp -raf rootfs/*  tmpfs/

#取消镜像文件的挂载。
umount tmpfs

#将制作的根文件系统镜像，打成gzip包。
gzip --best -c ramdisk.ext4 > ramdisk.ext4.gz

: '
将压缩了的ramdisk根文件系统二进制文件ramdisk.ext4.gz，转换成u-boot能够辨认的二进制文件ramdisk.ext4.img，
并指定ramdisk.ext4.img的名字为“ramdisk”，处理器体系架构为arm，操作系统类型为linux，程序类型为ramdisk，
程序由gzip压缩，不需要指定ramdisk的链接起始地址和入口地址。
'
mkimage -n "ramdisk" -A arm -O linux -T ramdisk -C gzip -d ramdisk.ext4.gz ramdisk.ext4.img

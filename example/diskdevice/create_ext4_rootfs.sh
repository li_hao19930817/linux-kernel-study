#!/bin/sh

busybox_folder="/home/lihao/Study/Busybox"
rootfs="rootfs"

#判断rootfs目录是否存在，不存在则新建。
if [ ! -d $rootfs ]; then
	mkdir $rootfs
fi

#将busybox编译生成的文件全部拷贝到rootfs目录中。
cp $busybox_folder/busybox1.33.2_install/*  $rootfs/ -rf
cd $rootfs

#补充根目录缺失的其他目录。
if [ ! -d proc ] && [ ! -d sys ] && [ ! -d dev ] && [ ! -d etc/init.d ]; then
	mkdir proc sys dev mnt home home/root etc etc/init.d
fi

#判断etc/init.d/rcS文件是否存在，如果存在则删除。
if [ -f etc/init.d/rcS ]; then
	rm etc/init.d/rcS
fi

#创建etc/init.d/rcS文件，并向其中写入系统启动是的配置文件。
echo "#!/bin/sh" > etc/init.d/rcS
echo "mount -t proc none /proc" >> etc/init.d/rcS
echo "mount -t sysfs none /sys" >> etc/init.d/rcS
echo "/sbin/mdev -s" >> etc/init.d/rcS

#为rcS添加可执行权限。
chmod +x etc/init.d/rcS

#如果文件系统rootfs.img存在，那么就先删除旧的rootfs.img。
if [ -f $busybox_folder/rootfs.ext4 ]; then
	rm $busybox_folder/rootfs.ext4
fi

#使用dd命令生成一个32M的镜像文件，并格式化为ext4文件系统。
dd if=/dev/zero of=$busybox_folder/rootfs.ext4 bs=1M count=32
mkfs.ext4 $busybox_folder/rootfs.ext4

#如果没有挂载ext4文件系统的临时目录，则重新创建。
if [ ! -d $busybox_folder/tmpfs ]; then
	mkdir $busybox_folder/tmpfs
fi

#将rootfs.ext4文件系统挂载到tmpfs目录下
mount -t ext4 $busybox_folder/rootfs.ext4 $busybox_folder/tmpfs -o loop

#将rootfs目录中的所有文件拷贝到挂载的rootfs.ext4文件系统中
cp -r $busybox_folder/rootfs/*  $busybox_folder/tmpfs/

#卸载tmpfs目录下挂载的文件系统
sudo umount $busybox_folder/tmpfs
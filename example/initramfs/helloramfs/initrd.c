#include <stdio.h>
#include <string.h>

#define BUFF_SIZE 1024

static const char* str[] = {
	"*******************************************\n",
	"*                                         *\n",
	"*         the easiest file system         *\n",
	"*                                         *\n",
	"*******************************************\n"
};

int main()
{
	for(int i=0;i<5;i++)
	{
		printf("%s",str[i]);
	}
	printf("root@linux:");
	
	fflush(stdin);
	char buf[BUFF_SIZE] = {0};
	while(1)
	{
		scanf("%s",buf);
		getchar();
		
		printf("%s\n",buf);
		printf("root@linux:");
	}
	
	return 0;
}
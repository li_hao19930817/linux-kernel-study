#!/bin/sh

busybox_folder="/home/lihao/Study/Busybox"
rootfs="rootfs"

#判断rootfs目录是否存在，不存在则新建。
if [ ! -d $rootfs ]; then
	mkdir $rootfs
fi

#将busybox编译生成的文件全部拷贝到rootfs目录中。
cp $busybox_folder/busybox1.33.2_install/*  $rootfs/ -rf
cd $rootfs

#补充根目录缺失的其他目录。
if [ ! -d proc ] && [ ! -d sys ] && [ ! -d dev ] && [ ! -d etc/init.d ]; then
	mkdir proc sys dev mnt home home/root etc etc/init.d
fi

#判断etc/init.d/rcS文件是否存在，如果存在则删除。
if [ -f etc/init.d/rcS ]; then
	rm etc/init.d/rcS
fi

#创建etc/init.d/rcS文件，并向其中写入系统启动是的配置文件。
echo "#!/bin/sh" > etc/init.d/rcS
echo "mount -t proc none /proc" >> etc/init.d/rcS
echo "mount -t sysfs none /sys" >> etc/init.d/rcS
echo "/sbin/mdev -s" >> etc/init.d/rcS

#为rcS添加可执行权限。
chmod +x etc/init.d/rcS

#如果文件系统rootfs.img存在，那么就先删除旧的rootfs.img。
if [ -f ~/Study/Busybox/rootfs.img ]; then
	rm ~/Study/Busybox/rootfs.img
fi

#把rootfs目录中的所有文件存档到rootfs.img。
find . | cpio -o --format=newc > ~/Study/Busybox/rootfs.img
/*1.使用qemu启动内核命令：
（1）qemu可以绕过uboot直接指定内核启动，让我们暂且不用去深究uboot启动内核的细节。
     即没有bootloader和文件系统的单独内核，也可以使用qemu跑起来。
（2）内核运行起来后会提示："end Kernel panic - not syncing: VFS: Unable to mount root fs on unknown-block(0,0)"错误。
     这是因为linux kernel没有找到VFS文件系统。
（3）arm linux内核：
qemu-system-arm -M vexpress-a9  \
                -m 512M  \
                -kernel ./kernel/Image.gz  \
                -nographic  \
                -append "console=ttyAMA0"
				
（4）arm64 linux内核：
qemu-system-aarch64 -cpu cortex-a57  \
                    -machine virt  \
                    -m 1024M  \
                    -kernel ./kernel/Image.gz  \
                    -append "console=ttyAMA0"  \
                    -nographic
（5）qemu以全系统仿真模式运行常用的命令选项：
  ① -cpu：用于指定CPU架构
  ② -machine/-M：用于指定开发板类型。
  ③ -kernel：用于指定内核镜像。
  ④ --append：用于指定一些附加选项，传递给linux内核。
        init=：用于指定文件系统起来后执行的linux的第一个进程
        root=：用于指定使用哪个设备作为根文件系统
     console=：用于指定终端。tty配置错的话将看不到打印输出。
  ⑤ -initrd：用于设置linux内核的initrd参数，可以指定文件系统。
  ⑥ -dtb：用于指定设备树描述二进制.dtb文件。
  -nographic：表示不使用图形界面。这样qemu将在当前shell下输出linux运行信息。
*/

/*2.使用qemu启动引导initramfs文件系统
（1）制作一个最小的initrd（initial ramdisk）：
	 vim init.c
	 
     #include <stdio.h>
     void main()
     {
     	printf("welcome to qemu test init\n");
     	while(1);
     }
（2）编译init.c得到了一个init可执行文件：
	 aarch64-linux-gnu-gcc -static -o init init.c     //使用静态编译，把库给衔接上
（3）将init可执行文件制作为cpio，即打包制作为initramfs文件系统：
	 echo init | cpio -o --format=newc > initramfs
（4）使用qemu "-initrd"选项引导initramfs文件系统启动：
qemu-system-aarch64 -cpu cortex-a57  \
                    -machine virt  \
                    -m 1024M  \
                    -kernel kernel/Image  \
                    -initrd fs/initramfs  \
                    -append "init=/init console=ttyAMA0"  \
                    -nographic
（5）qemu启动initramfs文件系统选项：
  ① init=/init：表明内核启动后引导的第一个应用程序。
  ② -initrd fs/initramfs：表示指定initrd。initramfs就是刚才我们制作的initrd。
*/

#include <stdio.h>

int main(void)
{
	printf("welcome to qemu test init\n");
	while(1);                               //进入死循环，阻塞程序避免退出。
	return 0;
}